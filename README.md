# An express API to calculate things, awesome!
This readme and project is a work in progress.

## Architecture

## Git practices
This project is based on Git Trunk practices: the main branch is trunk. Any ticket should have its own branch. When a ticket is ready to be reviewed, please create a Pull Request with the name of the ticket, and "Squash commits" ticked.

## Best practices
Here are a set of best practices for this project :
### Folder structure
This project is structured by feature. Then, each feature should be structured with a Router/Controller/Service/Models architecture.
Every Router, Controller and Service should be named following this pattern: <responsibility>.<feature>.ts

### Components and responsibilities
Routers should be declared as const and exported to be used by the app.

Controllers are responsible for ensuring that each query matches the structure it is expected to have. If a query is malformed, the controller should respond adequately.

A controller is a class containing only static functions that can be used by the corresponding router.

The services layer should not know about request and responses. It is only responsible for computing what needs to be computed.

### Naming
